import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MessageList from './messages/MessageList';
import {getChatLog, getMemberData} from './service';

import './App.css';

class App extends Component {

    componentWillMount() {
        this.props.getChatLog().then(this.props.getMemberData);
    }

    render() {
        return <MessageList/>;
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ getChatLog, getMemberData }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(App);
