import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

jest.mock('./messages/MessageList', () =>
  () => <div>test</div>
);

it('should render without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App.WrappedComponent getChatLog={() => new Promise(() => {})} />, div);
});
