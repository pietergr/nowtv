import React from 'react';
import {connect} from 'react-redux';

const MessageList = ({messages}) => <table>
    <tbody>
    {messages.map((m, i) => <tr key={i} title={m.member.email}>
        <td>
            <img alt={m.member.firstName} src={m.member.avatar}/>
        </td>
        <td>
            <em><small>{m.timestamp.format("dddd, Do MMMM YYYY, h:mm:ss a")}</small></em>
        </td>
        <td>
            {m.message}
        </td>
    </tr>)}
    </tbody>
</table>;


const mapStateToProps = state => {
    return {
        messages: state.messages || []
    };
};

export default connect(
    mapStateToProps
)(MessageList);
