import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import moment from 'moment';

export function reducer(state, action = {}) {
    switch(action.type) {
        case 'MEMBERS_LOADING_FULFILLED':
            const members = action.payload;
            return {
                ...state,
                messages: state.messages.map(m => ({
                    ...m,
                    member: members.filter(mem => mem.id === m.userId).length
                        ? members.filter(mem => mem.id === m.userId)[0] : {}
                }))
            };
        case 'MESSAGES_LOADING_FULFILLED':
            return {
                ...state,
                messages: action.payload.map(m => ({
                    ...m,
                    timestamp: moment(m.timestamp, 'YYYY-MM-DD HH:mm:ss ')
                })).sort((a, b) => a.timestamp.isBefore(b.timestamp) ? 1 : (a.timestamp.isAfter(b.timestamp) ? -1 : 0))
            };
        default:
            return state;
    }
}

export const store = createStore(reducer, {}, applyMiddleware(
  promiseMiddleware()
));