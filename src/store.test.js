import { reducer } from './store';

it('should set messages in the store', () => {
  const messages = [{
    id: 'cd445e6d-e514-424f-ba8f-16ec842002c6',
    userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
    message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
    timestamp: '2017-02-09T04:27:38Z'
  }];

  const updatedStore = reducer({}, { type: 'MESSAGES_LOADING_FULFILLED', payload: messages });

  const updatedMessagesWithoutTimestamp = updatedStore.messages.map(m => ({...m, timestamp: undefined}));

  expect(updatedMessagesWithoutTimestamp).toEqual([{
      id: 'cd445e6d-e514-424f-ba8f-16ec842002c6',
      userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
      message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.'
  }]);
});

it('should decorate messages with member data', () => {
    const messages = [{
        id: 'cd445e6d-e514-424f-ba8f-16ec842002c6',
        userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
        message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        timestamp: '2017-02-09T04:27:38Z'
    }];
    const members = [{
        "id": "fe27b760-a915-475c-80bb-7cdf14cc6ef3",
        "firstName": "Helen",
        "lastName": "Hawkins",
        "email": "hhawkins1@posterous.com",
        "avatar": "http://dummyimage.com/100x100.jpg/dddddd/000000",
        "ip": "179.239.189.173"
    }];

    let updatedStore = reducer({}, { type: 'MESSAGES_LOADING_FULFILLED', payload: messages });
    updatedStore = reducer(updatedStore, { type: 'MEMBERS_LOADING_FULFILLED', payload: members });

    const updatedMessages = updatedStore.messages.map(m => ({...m, timestamp: undefined}));

    expect(updatedMessages).toEqual([{
        id: 'cd445e6d-e514-424f-ba8f-16ec842002c6',
        userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
        message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
        member: {
            "id": "fe27b760-a915-475c-80bb-7cdf14cc6ef3",
            "firstName": "Helen",
            "lastName": "Hawkins",
            "email": "hhawkins1@posterous.com",
            "avatar": "http://dummyimage.com/100x100.jpg/dddddd/000000",
            "ip": "179.239.189.173"
        }
    }]);

    it('sorts messages in descending chronological order', () => {
        const messages = [
            {
                id: 'earlier',
                userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
                message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
                timestamp: '2017-02-09T04:27:38Z'
            },
            {
                id: 'later',
                userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
                message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
                timestamp: '2017-02-10T04:27:38Z'
            }
        ];
        let updatedStore = reducer({}, { type: 'MESSAGES_LOADING_FULFILLED', payload: messages });
        expect(updatedStore.messages[0].id).toEqual('later');
        expect(updatedStore.messages[1].id).toEqual('earlier');
    });

});